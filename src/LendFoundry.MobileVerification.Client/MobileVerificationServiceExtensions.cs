﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.MobileVerification.Client
{
    public static class MobileVerificationServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddSmsService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IMobileVerificationServiceFactory>(p => new MobileVerificationServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IMobileVerificationServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddSmsService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IMobileVerificationServiceFactory>(p => new MobileVerificationServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<IMobileVerificationServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddSmsService(this IServiceCollection services)
        {
            services.AddTransient<IMobileVerificationServiceFactory>(p => new MobileVerificationServiceFactory(p));
            services.AddTransient(p => p.GetService<IMobileVerificationServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
