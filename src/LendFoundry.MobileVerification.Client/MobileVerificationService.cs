﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;

namespace LendFoundry.MobileVerification.Client
{
    public class MobileVerificationService : IMobileVerificationService
    {
        private IServiceClient Client { get; }

        public MobileVerificationService(IServiceClient client)
        {
            Client = client;
        }

        public async Task<VerificationInitiateResponse> Initiate(string entityType, string entityId, IMobileVerificationRequest recipient)
        {
            return await Client.PostAsync<IMobileVerificationRequest, VerificationInitiateResponse>($"{entityType}/{entityId}", recipient, true);
        }

        public async Task<VerifyEmailResponse> Verify(string referenceNumber, string verificationCode)
        {
            return await Client.PostAsync<object, VerifyEmailResponse>($"verify/{referenceNumber}/{verificationCode}", null);
        }

        public async Task<IMobileVerifications> GetMobileVerification(string referenceNumber)
        {
            return await Client.GetAsync<MobileVerifications>($"{referenceNumber}");
        }
    }
}
