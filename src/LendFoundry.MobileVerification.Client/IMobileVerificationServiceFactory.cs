﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.MobileVerification.Client
{
    public interface IMobileVerificationServiceFactory
    {
        IMobileVerificationService Create(ITokenReader reader);
    }
}
