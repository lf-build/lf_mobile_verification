﻿using LendFoundry.Configuration.Client;
using LendFoundry.MobileVerification.Configuration;
using LendFoundry.MobileVerification.Providers;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using System;
using System.Runtime;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.MobileVerification.Persistence;
using LendFoundry.Configuration;
using LendFoundry.Sms.Client;

namespace LendFoundry.MobileVerification.Api
{
    /// <summary>
    /// Startup class
    /// </summary>
    internal class Startup
    {
        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
			 // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "MobileVerification"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.MobileVerification.Api.xml");
                var abstractionXmlPath = Path.Combine(basePath, "LendFoundry.MobileVerification.Abstractions.xml");
                c.IncludeXmlComments(xmlPath);
                c.IncludeXmlComments(abstractionXmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif            
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddSmsService();
            services.AddConfigurationService<MobileVerificationConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<MobileVerificationConfiguration>(Settings.ServiceName);

            services.AddTransient<IProviderFactory, ProviderFactory>();
            services.AddTransient<IMobileVerificationService, MobileVerificationService>();
            services.AddTransient<IMobileVerificationRepository, MobileVerificationRepository>();
            services.AddTransient<IMobileVerificationConfiguration>((pro) => pro.GetService<IConfigurationService<MobileVerificationConfiguration>>().Get());


            services.AddMvc().AddLendFoundryJsonOptions();

            services.AddCors();
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "MobileVerification Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}