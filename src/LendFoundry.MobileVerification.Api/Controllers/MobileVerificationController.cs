﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

using System;

namespace LendFoundry.MobileVerification.Api.Controllers
{
    /// <summary>
    /// MobileVerificationController class
    /// </summary>
    /// <seealso cref="ExtendedController" />
    [Route("/")]
    public class MobileVerificationController : ExtendedController
    {
        private IMobileVerificationService MobileVerificationService { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MobileVerificationController"/> class.
        /// </summary>
        /// <param name="smsService">The SMS service.</param>
        public MobileVerificationController(IMobileVerificationService smsService)
        {
            MobileVerificationService = smsService;
        }

        /// <summary>
        /// Initiates the mobile verification.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}")]
        [ProducesResponseType(typeof(VerificationInitiateResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> InitiateMobileVerification([FromRoute] string entityType,
                                                                    [FromRoute] string entityId,
                                                                    [FromBody] MobileVerificationRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await MobileVerificationService.Initiate(entityType, entityId, request));
                }
                catch (MobileVerificationFailedException ex)
                {
                    return ErrorResult.NotFound(ex.Message);
                }
                catch (ArgumentNullException ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
                catch (FormatException ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Verifies the mobile.
        /// </summary>
        /// <param name="referenceNumber">The reference number.</param>
        /// <param name="verificationCode">The verification code.</param>
        /// <returns></returns>
        [HttpPost("verify/{referenceNumber}/{verificationCode}")]
        [ProducesResponseType(typeof(VerifyEmailResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> VerifyMobile([FromRoute] string referenceNumber,
                                                      [FromRoute] string verificationCode)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await MobileVerificationService.Verify(referenceNumber, verificationCode));
                }
                catch (MobileVerificationException ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
                catch (MobileVerificationExpiredException ex)
                {
                    return ErrorResult.NotFound(ex.Message);
                }
                catch (MobileVerificationFailedException ex)
                {
                    return ErrorResult.NotFound(ex.Message);
                }
                catch (ArgumentNullException ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Gets Mobile Verification details based on reference number
        /// </summary>
        /// <param name="referenceNumber"></param>
        /// <returns></returns>
        [HttpGet("/{referenceNumber}")]
        [ProducesResponseType(typeof(VerifyEmailResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMobileVerification([FromRoute] string referenceNumber)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await MobileVerificationService.GetMobileVerification(referenceNumber));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
    }
}
