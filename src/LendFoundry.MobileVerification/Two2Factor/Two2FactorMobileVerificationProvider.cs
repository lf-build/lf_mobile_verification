﻿using LendFoundry.MobileVerification.Configuration;
using LendFoundry.MobileVerification.Providers;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.MobileVerification.Two2Factor
{
    public class Two2FactorMobileVerificationProvider : IProvider
    {
        private ITwo2FactorConfiguration Configuration { get; }

        public Two2FactorMobileVerificationProvider(ITwo2FactorConfiguration configuration)
        {
            Configuration = configuration;
        }

        public Task<VerificationInitiateResponse> InitiateMobileVerification(IMobileVerificationRequest recipient)
        {
            var taskResult = new TaskCompletionSource<VerificationInitiateResponse>();            
            var client = new RestClient(
                $"{Configuration.BaseUrl}/{Configuration.ApiKey}/{Configuration.OtpEndPoint.Replace("{phone_number}", recipient.Phone).Replace("{template_name}", Configuration.OtpTemplateName)}");
            var otpRequest = new RestRequest(Method.GET);
            client.ExecuteAsync<MobileVerificationResponse>(otpRequest, response =>
            {
                if (response != null && response.ResponseStatus == ResponseStatus.Completed)
                {
                    var obj = response.Content;
#if DOTNET2
                    var result = SimpleJson.DeserializeObject<MobileVerificationResponse>(obj);
#else
                    var result = SimpleJson.DeserializeObject<MobileVerificationResponse>(obj);
#endif
                    if (result.Status == "Success")
                    {
                        taskResult.SetResult(new VerificationInitiateResponse() { ReferenceNumber = result.Details });
                    }
                    else
                    {
                        taskResult.TrySetException(new MobileVerificationFailedException(result.Details));
                    }
                }
                else
                {
                    taskResult.TrySetException(new Exception("Unhandled error occurred while sending Otp using Two2Factor."));
                }
            });
            return taskResult.Task;
        }

        public Task<VerifyEmailResponse> VerifyMobile(string referenceNumber, string verificationCode)
        {
            var taskResult = new TaskCompletionSource<VerifyEmailResponse>();
            var client = new RestClient(string.Format("{0}/{1}/{2}", Configuration.BaseUrl, Configuration.ApiKey, Configuration.VerifyOtpEndPoint.Replace("{session_id}", referenceNumber).Replace("{otp_input}", verificationCode)));
            var request = new RestRequest(Method.GET);
            client.ExecuteAsync<MobileVerificationResponse>(request, response =>
            {
                if (response != null && response.ResponseStatus == ResponseStatus.Completed)
                {
                    var obj = response.Content;
#if DOTNET2
                    var result = SimpleJson.DeserializeObject<MobileVerificationResponse>(obj);
#else
                     var result = SimpleJson.DeserializeObject<MobileVerificationResponse>(obj);
#endif

                    if (result.Status == "Success")
                    {
                        if (result.Details.IndexOf("OTP Expired", StringComparison.InvariantCultureIgnoreCase) > -1)
                        {
                            taskResult.TrySetException(new MobileVerificationExpiredException("Otp Expired"));
                        }
                        else
                        {
                            taskResult.SetResult(new VerifyEmailResponse() { VerificationSuccess = true });
                        }
                    }
                    else
                    {
                        taskResult.TrySetException(new MobileVerificationFailedException(result.Details));
                    }
                }
                else
                {
                    taskResult.TrySetException(new MobileVerificationException("Unhandled error occurred while verifying Otp using Two2Factor."));
                }                
            });
            return taskResult.Task;
        }
    }
}
