﻿using System;
using Newtonsoft.Json.Linq;
using LendFoundry.MobileVerification.Configuration;
using LendFoundry.MobileVerification.Two2Factor;
using LendFoundry.MobileVerification.Providers;

namespace LendFoundry.MobileVerification
{
    public class ProviderFactory : IProviderFactory
    {
        public ProviderFactory(IMobileVerificationConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IMobileVerificationConfiguration Configuration { get;  }

        public IProvider GetProvider()
        {
            switch (Configuration.MobileVerificationProvider)
            {
                case MobileVerificationProviders.Two2Factor:                    
                        var two2FactorSettings = JObject.FromObject(Configuration.MobileVerificationSettings).ToObject<Two2FactorConfiguration>();
                        return new Two2FactorMobileVerificationProvider(two2FactorSettings);                    
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
