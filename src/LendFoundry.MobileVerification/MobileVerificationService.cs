﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.MobileVerification.Providers;
using LendFoundry.Foundation.Logging;
using LendFoundry.EventHub;
using LendFoundry.MobileVerification.Events;
using LendFoundry.Foundation.Date;
using LendFoundry.MobileVerification.Configuration;
using LendFoundry.Sms;
using Microsoft.CSharp.RuntimeBinder;
using LendFoundry.Foundation.Client;

namespace LendFoundry.MobileVerification
{
    public class MobileVerificationService : IMobileVerificationService
    {
        public IProviderFactory ProviderFactory { get; set; }
        private IEventHubClient EventHub { get; }
        private IMobileVerificationRepository Repository { get; }
        private ITenantTime TenantTime { get; }
        private ISmsService SmsService { get; }

        IMobileVerificationConfiguration Configuration;
        private ILogger Logger { get; }

        public MobileVerificationService(IProviderFactory providerFactory,
                                IEventHubClient eventHub,
                                IMobileVerificationRepository repository,
                                ITenantTime tenantTime,
                                IMobileVerificationConfiguration configuration,
                                ILogger logger,
                                ISmsService smsService)
        {
            ProviderFactory = providerFactory;
            EventHub = eventHub;
            Repository = repository;
            TenantTime = tenantTime;
            Configuration = configuration;
            Logger = logger;
            SmsService = smsService;
        }

        public async Task<VerificationInitiateResponse> Initiate(string entityType, string entityId, IMobileVerificationRequest recipient)
        {
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentNullException("Argument is null or empty", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentNullException("Argument is null or empty", nameof(entityId));
            if (recipient == null)
                throw new ArgumentNullException("Argument is null", nameof(recipient));

            if (string.IsNullOrEmpty(recipient.Phone))
                throw new ArgumentNullException("Argument is null", nameof(recipient.Phone));

            if (!IsValidPhone(recipient.Phone))
                throw new FormatException("Invalid phone number format");

            VerificationInitiateResponse response = null;
            if(Configuration.MobileVerificationProvider == MobileVerificationProviders.Two2Factor)
            {
                var mobileVerificationProvider = ProviderFactory.GetProvider();
                var result = await mobileVerificationProvider.InitiateMobileVerification(recipient);
                response = result;            
            }
            else
            {
                SmsResult result = null;
                if(!recipient.IsResend)
                {
                    result = await SmsService.SendOtp(entityType, entityId, new OtpSendRequest(){ Mobile = string.IsNullOrWhiteSpace(Configuration.PhonePrefix) ? recipient.Phone : recipient.Phone.Replace(Configuration.PhonePrefix, string.Empty), Email = GetEmailAddress(recipient.Data) });
                }
                else
                {
                    result = await SmsService.ResendOtp(entityType, entityId, new OtpResendRequest(){ Mobile = string.IsNullOrWhiteSpace(Configuration.PhonePrefix) ? recipient.Phone : recipient.Phone.Replace(Configuration.PhonePrefix, string.Empty), RetryType = GetRetryType(recipient.Data) == "voice" ? RetryType.voice : RetryType.text});
                }
                response = new VerificationInitiateResponse(){ ReferenceNumber = Guid.NewGuid().ToString("N") };
            }

            Repository.Add(new MobileVerifications()
            {
                EntityId = entityId,
                EntityType = entityType,
                ReferenceNumber = response.ReferenceNumber,
                CodeCreationTime = TenantTime.Now,
                CodeVerifiedOn = null,
                Phone = recipient.Phone,
                VerificationStatus = MobileVerificationStatus.Sent
            });

            await EventHub.Publish(new MobileVerificationInitiated
            {
                EntityType = entityType,
                EntityId = entityId,
                ReferenceNumber = response.ReferenceNumber,
                Request = recipient,
                Response = response
            });

            return response;
        }

        public async Task<VerifyEmailResponse> Verify(string referenceNumber, string verificationCode)
        {
            if (string.IsNullOrWhiteSpace(referenceNumber))
                throw new ArgumentNullException("Argument is null or whitespace", nameof(referenceNumber));
            if (string.IsNullOrWhiteSpace(verificationCode))
                throw new ArgumentNullException("Argument is null or whitespace", nameof(verificationCode));

            var mobileVerificationRecord = await Repository.GetMobileVerification(referenceNumber);

            if (mobileVerificationRecord == null)
                throw new ArgumentException($"Mobile verification details does not exist with reference number {referenceNumber}");

            if (mobileVerificationRecord.VerificationStatus == MobileVerificationStatus.Verified ||
               mobileVerificationRecord.VerificationStatus == MobileVerificationStatus.Expired)
            {
                throw new MobileVerificationException("Mobile already verified with given verification code or verification code has expired");
            }

            if (mobileVerificationRecord.VerificationAttempts < Configuration.MaxAttemptsAllowed)
            {
                VerifyEmailResponse response;

                mobileVerificationRecord.VerificationAttempts += 1;
                try
                {
                    if(Configuration.MobileVerificationProvider == MobileVerificationProviders.Two2Factor)
                    {
                        var mobileVerificationProvider = ProviderFactory.GetProvider();
                        response = await mobileVerificationProvider.VerifyMobile(referenceNumber, verificationCode);
                    }
                    else
                    {
                        var result = await SmsService.VerifyOtp(mobileVerificationRecord.EntityType, mobileVerificationRecord.EntityId, new OtpVerifyRequest() { Mobile = string.IsNullOrWhiteSpace(Configuration.PhonePrefix) ? mobileVerificationRecord.Phone : mobileVerificationRecord.Phone.Replace(Configuration.PhonePrefix, string.Empty), Otp = verificationCode });
                        response = new VerifyEmailResponse() { VerificationSuccess = true };                              
                    }

                    mobileVerificationRecord.VerificationStatus = MobileVerificationStatus.Verified;
                    mobileVerificationRecord.CodeVerifiedOn = TenantTime.Now;

                    Repository.Update(mobileVerificationRecord);

                    await EventHub.Publish(new MobileVerificationSucceeded
                    {
                        EntityType = mobileVerificationRecord.EntityType,
                        EntityId = mobileVerificationRecord.EntityId,
                        ReferenceNumber = referenceNumber,
                        Request = new { ReferenceNumber = referenceNumber, verificationCode },
                        Response = response
                    });

                    return response;
                }
                catch (Exception ex)
                {
                    if (ex.GetType() == typeof(MobileVerificationExpiredException) ||
                        ex.GetType() == typeof(MobileVerificationFailedException) ||
                        ex.GetType() == typeof(ClientException))
                    {
                        if (ex.GetType() == typeof(MobileVerificationExpiredException))
                        {
                            mobileVerificationRecord.VerificationStatus = MobileVerificationStatus.Expired;
                        }
                        mobileVerificationRecord.CodeVerifiedOn = TenantTime.Now;
                        Repository.Update(mobileVerificationRecord);

                        await EventHub.Publish(new MobileVerificationFailed
                        {
                            EntityType = mobileVerificationRecord.EntityType,
                            EntityId = mobileVerificationRecord.EntityId,
                            ReferenceNumber = referenceNumber,
                            Request = new { ReferenceNumber = referenceNumber, verificationCode },
                            Response = ex.Message
                        });
                    }
                    throw ex;
                }
            }
            else
            {
                if (mobileVerificationRecord.VerificationStatus == MobileVerificationStatus.Sent)
                {
                    mobileVerificationRecord.VerificationStatus = MobileVerificationStatus.Expired;
                    Repository.Update(mobileVerificationRecord);
                }
                throw new MobileVerificationException("Number of verification attempts exceeded maximum verification attempts allowed");
            }
        }

        public async Task<IMobileVerifications> GetMobileVerification(string referenceNumber)
        {
            if (string.IsNullOrWhiteSpace(referenceNumber))
                throw new ArgumentNullException(nameof(referenceNumber));

            var result = await Repository.GetMobileVerification(referenceNumber);

            if (result == null)
                throw new ArgumentException($"No mobile verification details found for reference number {referenceNumber}");

            return result;
        }

        private static bool IsValidPhone(string recipientPhone)
        {
            return Regex.IsMatch(recipientPhone,
                @"^\+(?:[0-9]●?){6,14}[0-9]$",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        private static string GetEmailAddress(dynamic data)
        {
            if(data == null)
                return string.Empty;
            Func<dynamic> emailProperty = () => data.Email;
            if (!HasProperty(emailProperty))
            {
                emailProperty = () => data.email;
            }
            var email = GetValue(emailProperty);
            return Convert.ToString(email);
        }

        private static string GetRetryType(dynamic data)
        {
            if(data == null)
                return string.Empty;
            Func<dynamic> retryTypeProperty = () => data.RetryType;
            if (!HasProperty(retryTypeProperty))
            {
                retryTypeProperty = () => data.retryType;
            }
            var retryType = GetValue(retryTypeProperty);
            return Convert.ToString(retryType);
        }

        private static bool HasProperty<T>(Func<T> @property)
        {
            try
            {
                @property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> @property)
        {
            return @property();
        }
    }
}
