﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;

namespace LendFoundry.MobileVerification.Persistence
{
    public class MobileVerificationRepository : MongoRepository<IMobileVerifications, MobileVerifications>, IMobileVerificationRepository
    {
        static MobileVerificationRepository()
        {
            BsonClassMap.RegisterClassMap<MobileVerifications>(map =>
            {
                map.AutoMap();
                var type = typeof(MobileVerifications);
                map.MapProperty(p => p.VerificationStatus).SetSerializer(new EnumSerializer<MobileVerificationStatus>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public MobileVerificationRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "mobile-verification")
        {
            CreateIndexIfNotExists("ReferenceNumber", Builders<IMobileVerifications>.IndexKeys.Ascending(x => x.ReferenceNumber));
        }

        public async Task<IMobileVerifications> GetMobileVerification(string referenceNumber)
        {
            return await Query.FirstOrDefaultAsync(x => x.ReferenceNumber == referenceNumber);
        }
    }
}
