﻿using System.Threading.Tasks;

namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// IMobileVerificationService
    /// </summary>
    public interface IMobileVerificationService
    {
        /// <summary>
        /// Initiates the specified entity type.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="recipient">The recipient.</param>
        /// <returns></returns>
        Task<VerificationInitiateResponse> Initiate(string entityType, string entityId, IMobileVerificationRequest recipient);
        /// <summary>
        /// Verifies the specified reference number.
        /// </summary>
        /// <param name="referenceNumber">The reference number.</param>
        /// <param name="verificationCode">The verification code.</param>
        /// <returns></returns>
        Task<VerifyEmailResponse> Verify(string referenceNumber, string verificationCode);
        /// <summary>
        /// Gets the mobile verification.
        /// </summary>
        /// <param name="referenceNumber">The reference number.</param>
        /// <returns></returns>
        Task<IMobileVerifications> GetMobileVerification(string referenceNumber);
    }
}
