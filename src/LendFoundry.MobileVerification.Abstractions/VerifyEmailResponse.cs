﻿namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// VerifyEmailResponse
    /// </summary>
    public class VerifyEmailResponse
    {
        /// <summary>
        /// Gets or sets a value indicating whether [verification success].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [verification success]; otherwise, <c>false</c>.
        /// </value>
        public bool VerificationSuccess { get; set; }
    }
}
