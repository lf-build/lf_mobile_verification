﻿using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// IMobileVerificationRepository
    /// </summary>
    public interface IMobileVerificationRepository : IRepository<IMobileVerifications>
    {
        /// <summary>
        /// Gets the mobile verification.
        /// </summary>
        /// <param name="referenceNumber">The reference number.</param>
        /// <returns></returns>
        Task<IMobileVerifications> GetMobileVerification(string referenceNumber);
    }
}