﻿namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// MobileVerificationStatus
    /// </summary>
    public enum MobileVerificationStatus
    {
        /// <summary>
        /// The sent
        /// </summary>
        Sent,
        /// <summary>
        /// The expired
        /// </summary>
        Expired,
        /// <summary>
        /// The verified
        /// </summary>
        Verified
    }
}
