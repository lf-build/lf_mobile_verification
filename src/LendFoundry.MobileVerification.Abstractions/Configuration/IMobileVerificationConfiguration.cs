﻿using LendFoundry.Foundation.Client;
using LendFoundry.MobileVerification.Providers;

namespace LendFoundry.MobileVerification.Configuration
{
    /// <summary>
    /// IMobileVerificationConfiguration
    /// </summary>
    /// <seealso cref="LendFoundry.Foundation.Client.IDependencyConfiguration" />
    public interface IMobileVerificationConfiguration : IDependencyConfiguration
    {
        /// <summary>
        /// Gets or sets the mobile verification provider.
        /// </summary>
        /// <value>
        /// The mobile verification provider.
        /// </value>
        MobileVerificationProviders MobileVerificationProvider { get; set; }
        /// <summary>
        /// Gets or sets the maximum attempts allowed.
        /// </summary>
        /// <value>
        /// The maximum attempts allowed.
        /// </value>
        int MaxAttemptsAllowed { get; set; }
        /// <summary>
        /// Gets or sets the mobile verification settings.
        /// </summary>
        /// <value>
        /// The mobile verification settings.
        /// </value>
        object MobileVerificationSettings { get; set; }
        /// <summary>
        /// Gets or sets the phone prefix.
        /// </summary>
        /// <value>
        /// The phone prefix.
        /// </value>
        string PhonePrefix { get; set; }
    }
}
