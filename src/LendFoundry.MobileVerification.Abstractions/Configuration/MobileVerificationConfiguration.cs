using LendFoundry.MobileVerification.Providers;
using System.Collections.Generic;

namespace LendFoundry.MobileVerification.Configuration
{
    /// <summary>
    /// MobileVerificationConfiguration
    /// </summary>
    /// <seealso cref="LendFoundry.MobileVerification.Configuration.IMobileVerificationConfiguration" />
    public class MobileVerificationConfiguration : IMobileVerificationConfiguration
    {
        /// <summary>
        /// Gets or sets the mobile verification provider.
        /// </summary>
        /// <value>
        /// The mobile verification provider.
        /// </value>
        public MobileVerificationProviders MobileVerificationProvider { get; set; }
        /// <summary>
        /// Gets or sets the maximum attempts allowed.
        /// </summary>
        /// <value>
        /// The maximum attempts allowed.
        /// </value>
        public int MaxAttemptsAllowed { get; set; }
        /// <summary>
        /// Gets or sets the mobile verification settings.
        /// </summary>
        /// <value>
        /// The mobile verification settings.
        /// </value>
        public object MobileVerificationSettings { get; set; }
        /// <summary>
        /// Gets or sets the dependencies.
        /// </summary>
        /// <value>
        /// The dependencies.
        /// </value>
        public Dictionary<string, string> Dependencies { get; set; }
        /// <summary>
        /// Gets or sets the database.
        /// </summary>
        /// <value>
        /// The database.
        /// </value>
        public string Database { get; set; }
        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        public string ConnectionString { get; set; }
        /// <summary>
        /// Gets or sets the phone prefix.
        /// </summary>
        /// <value>
        /// The phone prefix.
        /// </value>
        public string PhonePrefix { get; set; }
    }
}