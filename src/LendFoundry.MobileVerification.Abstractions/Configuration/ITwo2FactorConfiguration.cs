﻿namespace LendFoundry.MobileVerification.Configuration
{
    /// <summary>
    /// ITwo2FactorConfiguration
    /// </summary>
    public interface ITwo2FactorConfiguration
    {
        /// <summary>
        /// Gets or sets the base URL.
        /// </summary>
        /// <value>
        /// The base URL.
        /// </value>
        string BaseUrl { get; set; }
        /// <summary>
        /// Gets or sets the API key.
        /// </summary>
        /// <value>
        /// The API key.
        /// </value>
        string ApiKey { get; set; }
        /// <summary>
        /// Gets or sets the otp end point.
        /// </summary>
        /// <value>
        /// The otp end point.
        /// </value>
        string OtpEndPoint { get; set; }
        /// <summary>
        /// Gets or sets the otp check balance end point.
        /// </summary>
        /// <value>
        /// The otp check balance end point.
        /// </value>
        string OtpCheckBalanceEndPoint { get; set; }
        /// <summary>
        /// Gets or sets the verify otp end point.
        /// </summary>
        /// <value>
        /// The verify otp end point.
        /// </value>
        string VerifyOtpEndPoint { get; set; }
        /// <summary>
        /// Gets or sets the tr SMS end point.
        /// </summary>
        /// <value>
        /// The tr SMS end point.
        /// </value>
        string TrSmsEndPoint { get; set; }
        /// <summary>
        /// Gets or sets the tr SMS check balance end point.
        /// </summary>
        /// <value>
        /// The tr SMS check balance end point.
        /// </value>
        string TrSmsCheckBalanceEndPoint { get; set; }
        /// <summary>
        /// Gets or sets the tr SMS delivery report end point.
        /// </summary>
        /// <value>
        /// The tr SMS delivery report end point.
        /// </value>
        string TrSmsDeliveryReportEndPoint { get; set; }
        /// <summary>
        /// Gets or sets the name of the otp template.
        /// </summary>
        /// <value>
        /// The name of the otp template.
        /// </value>
        string OtpTemplateName { get; set; }
        /// <summary>
        /// Gets or sets the name of the sender.
        /// </summary>
        /// <value>
        /// The name of the sender.
        /// </value>
        string SenderName { get; set; }
    }
}
