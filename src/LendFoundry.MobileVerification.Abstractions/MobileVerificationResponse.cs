﻿namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// MobileVerificationResponse
    /// </summary>
    public class MobileVerificationResponse
    {
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status { get; set; }
        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
        public string Details { get; set; }
    }
}
