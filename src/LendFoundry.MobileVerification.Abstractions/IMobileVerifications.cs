﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// IMobileVerifications
    /// </summary>
    /// <seealso cref="LendFoundry.Foundation.Persistence.IAggregate" />
    public interface IMobileVerifications : IAggregate
    {
        /// <summary>
        /// Gets or sets the entity identifier.
        /// </summary>
        /// <value>
        /// The entity identifier.
        /// </value>
        string EntityId { get; set; }
        /// <summary>
        /// Gets or sets the type of the entity.
        /// </summary>
        /// <value>
        /// The type of the entity.
        /// </value>
        string EntityType { get; set; }
        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        string Phone { get; set; }
        /// <summary>
        /// Gets or sets the reference number.
        /// </summary>
        /// <value>
        /// The reference number.
        /// </value>
        string ReferenceNumber { get; set; }
        /// <summary>
        /// Gets or sets the code creation time.
        /// </summary>
        /// <value>
        /// The code creation time.
        /// </value>
        DateTimeOffset CodeCreationTime { get; set; }
        /// <summary>
        /// Gets or sets the code verified on.
        /// </summary>
        /// <value>
        /// The code verified on.
        /// </value>
        DateTimeOffset? CodeVerifiedOn { get; set; }
        /// <summary>
        /// Gets or sets the verification status.
        /// </summary>
        /// <value>
        /// The verification status.
        /// </value>
        MobileVerificationStatus VerificationStatus { get; set; }
        /// <summary>
        /// Gets or sets the verification attempts.
        /// </summary>
        /// <value>
        /// The verification attempts.
        /// </value>
        int VerificationAttempts { get; set; }
    }
}