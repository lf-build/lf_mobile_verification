﻿using System;

namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// Settings class
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        /// <value>
        /// The name of the service.
        /// </value>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "mobile-verification";
    }
}
