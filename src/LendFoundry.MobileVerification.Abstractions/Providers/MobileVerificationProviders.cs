﻿namespace LendFoundry.MobileVerification.Providers
{
    /// <summary>
    /// MobileVerificationProviders
    /// </summary>
    public enum MobileVerificationProviders
    {
        /// <summary>
        /// The two2 factor
        /// </summary>
        Two2Factor,
        /// <summary>
        /// Msg91
        /// </summary>
        Msg91
    }
}
