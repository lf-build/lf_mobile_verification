﻿using System.Threading.Tasks;

namespace LendFoundry.MobileVerification.Providers
{
    /// <summary>
    /// IProvider
    /// </summary>
    public interface IProvider
    {
        /// <summary>
        /// Initiates the mobile verification.
        /// </summary>
        /// <param name="recipient">The recipient.</param>
        /// <returns></returns>
        Task<VerificationInitiateResponse> InitiateMobileVerification(IMobileVerificationRequest recipient);
        /// <summary>
        /// Verifies the mobile.
        /// </summary>
        /// <param name="referenceNumber">The reference number.</param>
        /// <param name="verificationCode">The verification code.</param>
        /// <returns></returns>
        Task<VerifyEmailResponse> VerifyMobile(string referenceNumber, string verificationCode);
    }
}
