﻿namespace LendFoundry.MobileVerification.Providers
{
    /// <summary>
    /// IProviderFactory
    /// </summary>
    public interface IProviderFactory
    {
        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <returns></returns>
        IProvider GetProvider();
    }
}
