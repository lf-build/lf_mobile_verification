﻿namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// VerificationInitiateResponse
    /// </summary>
    public class VerificationInitiateResponse
    {
        /// <summary>
        /// Gets or sets the reference number.
        /// </summary>
        /// <value>
        /// The reference number.
        /// </value>
        public string ReferenceNumber { get; set; }
    }
}
