﻿namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// MobileVerificationRequest
    /// </summary>
    public class MobileVerificationRequest : IMobileVerificationRequest
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Mobile
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// Data as per template
        /// </summary>
        public object Data { get; set; }
        /// <summary>
        /// Gets or sets the IsResend.
        /// </summary>
        public bool IsResend { get; set; }
    }
}
