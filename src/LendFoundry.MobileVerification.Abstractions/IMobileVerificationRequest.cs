﻿namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// IMobileVerificationRequest
    /// </summary>
    public interface IMobileVerificationRequest
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; set; }
        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        string Phone { get; set; }
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>
        /// The mobile.
        /// </value>
        string Mobile { get; set; }
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        object Data { get; set; }
        /// <summary>
        /// Gets or sets the IsResend.
        /// </summary>
        /// <value>
        /// The IsResend.
        /// </value>
        bool IsResend { get; set; }
    }
}
