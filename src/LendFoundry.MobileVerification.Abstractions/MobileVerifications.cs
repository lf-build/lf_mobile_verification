﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.MobileVerification
{
    /// <summary>
    /// MobileVerifications
    /// </summary>
    /// <seealso cref="LendFoundry.Foundation.Persistence.Aggregate" />
    /// <seealso cref="LendFoundry.MobileVerification.IMobileVerifications" />
    public class MobileVerifications : Aggregate, IMobileVerifications
    {
        /// <summary>
        /// Gets or sets the entity identifier.
        /// </summary>
        /// <value>
        /// The entity identifier.
        /// </value>
        public string EntityId { get; set; }
        /// <summary>
        /// Gets or sets the type of the entity.
        /// </summary>
        /// <value>
        /// The type of the entity.
        /// </value>
        public string EntityType { get; set; }
        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        public string Phone { get; set; }
        /// <summary>
        /// Gets or sets the reference number.
        /// </summary>
        /// <value>
        /// The reference number.
        /// </value>
        public string ReferenceNumber { get; set; }
        /// <summary>
        /// Gets or sets the code creation time.
        /// </summary>
        /// <value>
        /// The code creation time.
        /// </value>
        public DateTimeOffset CodeCreationTime { get; set; }
        /// <summary>
        /// Gets or sets the code verified on.
        /// </summary>
        /// <value>
        /// The code verified on.
        /// </value>
        public DateTimeOffset? CodeVerifiedOn { get; set; }
        /// <summary>
        /// Gets or sets the verification status.
        /// </summary>
        /// <value>
        /// The verification status.
        /// </value>
        public MobileVerificationStatus VerificationStatus { get; set; }
        /// <summary>
        /// Gets or sets the verification attempts.
        /// </summary>
        /// <value>
        /// The verification attempts.
        /// </value>
        public int VerificationAttempts { get; set; }
    }
}
