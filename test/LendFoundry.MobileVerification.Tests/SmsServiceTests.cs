﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.MobileVerification.Providers;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
using Moq;
using Xunit;
using LendFoundry.Foundation.Logging;
using LendFoundry.MobileVerification.Configuration;
using LendFoundry.Tenant.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.MobileVerification.Events;
using LendFoundry.MobileVerification;
using LendFoundry.Foundation.Date;

namespace CreditExchange.MobileOtp.Tests
{
    public class SmsServiceTests
    {
        private Mock<IProviderFactory> SmsProviderFactory { get; }
        private Mock<ITemplateManagerService> TemplateManager { get; }
        private Mock<IProvider> SmsProvider { get; }
        private IMobileVerificationService SmsService { get; }
        private Mock<ILogger> Logger { get; }

        private Mock<ITenantTime> TenantService { get; }
        private Mock<IMobileVerificationRepository> Repository { get; }
        private Mock<IMobileVerificationConfiguration> Configuration { get; }
        private Mock<IEventHubClient> EventHubClient { get; }
        //  private Mock<ITokenHandler> TokenHandler { get; }

        public SmsServiceTests()
        {
            SmsProviderFactory = new Mock<IProviderFactory>();
            TemplateManager = new Mock<ITemplateManagerService>();
            SmsProvider = new Mock<IProvider>();
            Logger = new Mock<ILogger>();
            Repository = new Mock<IMobileVerificationRepository>();
            TenantService = new Mock<ITenantTime>();
            Configuration = new Mock<IMobileVerificationConfiguration>();
            EventHubClient = new Mock<IEventHubClient>();
            

            SmsProviderFactory.Setup(x => x.GetProvider())
                .Returns(SmsProvider.Object);

            SmsService = new MobileVerificationService(SmsProviderFactory.Object,
                                        EventHubClient.Object,
                                        Repository.Object,
                                        TenantService.Object,
                                        Configuration.Object,
                                        Logger.Object);
        }

        [Fact]
        public  void InitiateMobileVerification_NullArguments_ThrowsArgumentExceptions()
        {
            var data = new { RecipientPhone = "+918007333733" };
            var mobileRequest = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone ="", Name = "abc" };

             Assert.ThrowsAsync<ArgumentNullException>(() => SmsService.Initiate("" , "123" , mobileRequest));

            Assert.ThrowsAsync<ArgumentNullException>(() => SmsService.Initiate("application", "", mobileRequest));

            Assert.ThrowsAsync<ArgumentNullException>(() => SmsService.Initiate("application", "123", null));

            Assert.ThrowsAsync<ArgumentNullException>(() => SmsService.Initiate("application", "123", mobileRequest));

            var mobileRequestnotValid = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone = "12345", Name = "abc" };

            Assert.ThrowsAsync<ArgumentNullException>(() => SmsService.Initiate("application", "123", mobileRequestnotValid));
        }

        [Fact]
        public async void InitiateMobileVerification_WithValidAgruments()
        {
            var data = new { RecipientPhone = "+918007333733" };
           var mobileRequest = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone = "+918007333733", Name = "abc" };
            var response = new VerificationInitateResponse() { ReferenceNumber = "123" };
           
            SmsProviderFactory.Setup(x => x.GetProvider()).Returns(SmsProvider.Object);

            SmsProvider.Setup(x => x.InitiateMobileVerification(mobileRequest))
                .ReturnsAsync(response);

            Repository.Setup(x => x.Add(It.IsAny<MobileVerifications>()));
            Mock<IEventHubClient> eventHubClient = new Mock<IEventHubClient>();

            eventHubClient.Setup(x => x.Publish("SmsSentEvent", new MobileVerificationInitiated()
            {
                EntityType = "application",
                EntityId = "12305",
                 Request = mobileRequest,
                Response = "SMS sent successfully"
            })).ReturnsAsync(true);

            var result = await SmsService.Initiate("application", "123", mobileRequest);
            Assert.True(result == response);

        }


        [Fact]
        public async void InitiateMobileVerification_ThrowsUnknownException()
        {
            var response = new VerificationInitateResponse() { ReferenceNumber = "123" };
            var data = new { RecipientPhone = "+918007333733" };
           var mobileRequest = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone = "+918007333733", Name = "abc" };

            SmsProviderFactory.Setup(x => x.GetProvider()).Returns(SmsProvider.Object);

            SmsProvider.Setup(x => x.InitiateMobileVerification(It.IsAny<MobileVerificationRequest>()))
                .Throws(new Exception("Unknown Error"));

            await Assert.ThrowsAsync<Exception>(() => SmsService.Initiate("application", "123", mobileRequest));
        }

        [Fact]
        public async void InitiateMobileVerification_WithValidArgumentsAndData_VerifyCalls()
        {
            var response = new VerificationInitateResponse() { ReferenceNumber = "123" };
            var data = new { RecipientPhone = "+918007333733" };
           var mobileRequest = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone = "+918007333733", Name = "abc" };

            SmsProviderFactory.Setup(x => x.GetProvider()).Returns(SmsProvider.Object);

            SmsProvider.Setup(x => x.InitiateMobileVerification(It.IsAny<MobileVerificationRequest>()))
                .ReturnsAsync(response);

            Repository.Setup(x => x.Add(It.IsAny<MobileVerifications>()));
            Mock<IEventHubClient> eventHubClient = new Mock<IEventHubClient>();

            eventHubClient.Setup(x => x.Publish("SmsSentEvent", new MobileVerificationInitiated()
            {
                EntityType = "application",
                EntityId = "12305",
                Request = mobileRequest,
                Response = "SMS sent successfully"
            })).ReturnsAsync(true);

            var result = await SmsService.Initiate("application", "123", mobileRequest);

            Assert.True(result == response);
            SmsProviderFactory.Verify(x => x.GetProvider(), Times.Once);
            SmsProvider.Verify(x => x.InitiateMobileVerification(mobileRequest), Times.Once);
        }



        [Fact]
        public void VerifyMobile_InValidArguments_ThrowsArgumentExceptions()
        {         
            //null Arguments
            Assert.ThrowsAsync<ArgumentNullException>(() => SmsService.Verify("", "123456"));
            Assert.ThrowsAsync<ArgumentNullException>(() => SmsService.Verify("123", ""));

            //Wrong VerificationStatus
            var response = new MobileVerifications() { ReferenceNumber = "123", VerificationStatus = MobileVerificationStatus.Verified, VerificationAttempts = 3, EntityType = "123", EntityId = "123" };
            Repository.Setup(x => x.GetMobileVerification(It.IsAny<string>())).Returns(response);
            Repository.Setup(x => x.Update(It.IsAny<MobileVerifications>()));

            Mock<IEventHubClient> eventHubClient = new Mock<IEventHubClient>();
            eventHubClient.Setup(x => x.Publish("SmsSentEvent", new MobileVerificationInitiated()
            {
                EntityType = "application",
                EntityId = "12305",               
                Response = "SMS sent successfully"
            })).ReturnsAsync(true);

            Assert.ThrowsAsync<MobileVerificationException>(() => SmsService.Verify("123", "123456"));


            //With Attempt greater then Confugartion
            Configuration.SetupProperty(x => x.MaxAttemptsAllowed, 3);
            var responseWithMaxAttampt = new MobileVerifications() { ReferenceNumber = "123", VerificationStatus = MobileVerificationStatus.Sent, VerificationAttempts = 4, EntityType = "123", EntityId = "123" };
            Repository.Setup(x => x.GetMobileVerification(It.IsAny<string>())).Returns(responseWithMaxAttampt);
            Assert.ThrowsAsync<MobileVerificationException>(() => SmsService.Verify("123", "123456"));


            //  VerifyMobile throws  MobileVerificationExpiredException         
            SmsProviderFactory.Setup(x => x.GetProvider()).Returns(SmsProvider.Object);
            var responseWithValidAttempt = new MobileVerifications() { ReferenceNumber = "123", VerificationStatus = MobileVerificationStatus.Sent, VerificationAttempts = 2, EntityType = "123", EntityId = "123" };
            Repository.Setup(x => x.GetMobileVerification(It.IsAny<string>())).Returns(responseWithValidAttempt);
            SmsProvider.Setup(x => x.VerifyMobile(It.IsAny<string>(), It.IsAny<string>())).Throws(new MobileVerificationExpiredException());
            Assert.ThrowsAsync<MobileVerificationExpiredException>(() => SmsService.Verify("123", "123456"));

        }

        [Fact]
        public async void VerifyMobile_WithValidAgruments()
        {
            var response = new MobileVerifications() { ReferenceNumber = "123", VerificationStatus = MobileVerificationStatus.Sent, VerificationAttempts = 2, EntityType = "123", EntityId = "123" };
            Repository.Setup(x => x.GetMobileVerification(It.IsAny<string>())).Returns(response);
            Repository.Setup(x => x.Update(It.IsAny<MobileVerifications>()));

            Mock<IEventHubClient> eventHubClient = new Mock<IEventHubClient>();
            eventHubClient.Setup(x => x.Publish("SmsSentEvent", new MobileVerificationInitiated()
            {
                EntityType = "application",
                EntityId = "12305",
                Response = "SMS sent successfully"
            })).ReturnsAsync(true);

            var emailresoinse =  new VerifyEmailResponse() { VerificationSuccess = true };
            SmsProviderFactory.Setup(x => x.GetProvider()).Returns(SmsProvider.Object);
            SmsProvider.Setup(x => x.VerifyMobile(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emailresoinse);
            Configuration.SetupProperty(x => x.MaxAttemptsAllowed, 3);

            var result = await SmsService.Verify("123", "123456");
            Assert.True(result.VerificationSuccess == true);

        }


        [Fact]
        public async void VeriftOtp_ThrowsUnknownException()
        {
            var response = new MobileVerifications() { ReferenceNumber = "123", VerificationStatus = MobileVerificationStatus.Sent, VerificationAttempts = 2, EntityType = "123", EntityId = "123" };
            Repository.Setup(x => x.GetMobileVerification(It.IsAny<string>())).Returns(response);
            Repository.Setup(x => x.Update(It.IsAny<MobileVerifications>()));
            SmsProviderFactory.Setup(x => x.GetProvider()).Returns(SmsProvider.Object);
            SmsProvider.Setup(x => x.VerifyMobile(It.IsAny<string>(), It.IsAny<string>()))
              .Throws(new Exception("Unknown Error"));
            Configuration.SetupProperty(x => x.MaxAttemptsAllowed, 3);

            Mock<IEventHubClient> eventHubClient = new Mock<IEventHubClient>();
            eventHubClient.Setup(x => x.Publish("SmsSentEvent", new MobileVerificationInitiated()
            {
                EntityType = "application",
                EntityId = "12305",
                Response = "SMS sent successfully"
            })).ReturnsAsync(true);

            await Assert.ThrowsAsync<Exception>(() => SmsService.Verify("123", "123456"));
        }

      
    }
}
