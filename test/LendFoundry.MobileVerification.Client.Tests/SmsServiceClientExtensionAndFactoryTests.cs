﻿using System;
using System.Linq;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
using Moq;
using Xunit;

namespace LendFoundry.MobileVerification.Client.Tests
{
    public class SmsServiceClientExtensionAndFactoryTests
    {
        private Mock<IHttpContextAccessor> HttpContextAccessor { get; }
        private Mock<ILogger> Logger { get; }
        private Mock<ITokenReader> TokenReader { get; }
        private IServiceCollection ServiceCollection { get; }
        private IServiceProvider ServiceProvider { get; }

        public SmsServiceClientExtensionAndFactoryTests()
        {
            HttpContextAccessor = new Mock<IHttpContextAccessor>();
            Logger = new Mock<ILogger>();
            TokenReader = new Mock<ITokenReader>();

            ServiceCollection = new ServiceCollection();

            ServiceCollection.AddSingleton(_ => HttpContextAccessor.Object);
            ServiceCollection.AddSingleton(_ => Logger.Object);
            ServiceCollection.AddSingleton(_ => TokenReader.Object);

            ServiceProvider = ServiceCollection.BuildServiceProvider();
        }

        [Fact]
        public void Should_Be_Able_To_Create_SmsService()
        {
            var factory = new MobileVerificationServiceFactory(ServiceProvider, "10.1.1.99", 5000);
            var client = factory.Create(TokenReader.Object);
            Assert.NotNull(client);
            Assert.IsType<MobileVerificationService>(client);
        }

        [Fact]
        public void Should_Be_Able_To_AddSmsService()
        {
            var serviceCollection = MobileVerificationServiceExtensions.AddSmsService(ServiceCollection, "10.1.1.99", 5000);

            Assert.NotNull(serviceCollection);
            Assert.NotEmpty(serviceCollection);
            Assert.Equal(5, serviceCollection.Count);
            Assert.Equal(1, serviceCollection.Count(x => x.ServiceType == typeof(IMobileVerificationService)));
            Assert.Equal(1, serviceCollection.Count(x => x.ServiceType == typeof(IMobileVerificationServiceFactory)));
        }
    }
}
