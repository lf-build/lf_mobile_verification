﻿using LendFoundry.Foundation.Services;
using LendFoundry.MobileVerification;
using LendFoundry.MobileVerification.Client;
using Moq;
using RestSharp;
using Xunit;

namespace CreditExchange.MobileOtp.Client.Tests
{
    public class SmsServiceClientTests
    {
        private IMobileVerificationService Client { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> ServiceClient { get; }

        public SmsServiceClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new MobileVerificationService(ServiceClient.Object);
        }

        [Fact]
        public async void Client_SendOtp()
        {
            var data = new { RecipientPhone = "+918007333733" };
            var mobileRequest = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone = "+918007333733", Name = "abc" };
            var response = new VerificationInitateResponse() { ReferenceNumber = "123" };


            ServiceClient.Setup(x => x.ExecuteAsync<VerificationInitateResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(response);

            var result = await Client.Initiate("123", "123", mobileRequest);
            Assert.Equal("{entitytype}/{entityid}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_VerifyOtp()
        {
            var data = new { RecipientPhone = "+918007333733" };
            var mobileRequest = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone = "+918007333733", Name = "abc" };
            var response = new VerificationInitateResponse() { ReferenceNumber = "123" };
            var emailresoinse = new VerifyEmailResponse() { VerificationSuccess = true };

            ServiceClient.Setup(x => x.ExecuteAsync<VerifyEmailResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(emailresoinse);

            var result = await Client.Verify("123", "123456");
            Assert.Equal("verify/{referencenumber}/{verificationCode}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }
    }
}
