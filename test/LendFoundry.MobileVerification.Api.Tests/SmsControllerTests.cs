﻿using System;
using LendFoundry.MobileVerification.Api.Controllers;
using LendFoundry.MobileVerification.Providers;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;
using LendFoundry.Foundation.Logging;
using LendFoundry.MobileVerification.Configuration;
using LendFoundry.Tenant.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.MobileVerification.Events;
using LendFoundry.MobileVerification;

namespace CreditExchange.MobileOtp.Api.Tests
{
    public class SmsContollerTests
    {
        private Mock<IMobileVerificationService> mobileVerificationService { get; }
        private MobileVerificationController smsController { get; }


        public SmsContollerTests()
        {
            mobileVerificationService = new Mock<IMobileVerificationService>();
            smsController = new MobileVerificationController(mobileVerificationService.Object);
        }


        [Fact]
        public async void InitiateMobileVerification_Returns_Ok()
        {
            var data = new { RecipientPhone = "+918007333733" };
            var mobileRequest = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone = "+918007333733", Name = "abc" };

            var response = new VerificationInitateResponse() { ReferenceNumber = "123" };
            mobileVerificationService.Setup(x => x.Initiate(It.IsAny<string>(), It.IsAny<string>(), mobileRequest)).ReturnsAsync(response);
            var result = (HttpOkObjectResult)await smsController.InitiateMobileVerification("123", "123", mobileRequest);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
            //Assert.True(result.Value.GetType);
        }

        [Fact]
        public async void InitiateMobileVerification_Returns_MobileVerificationFailedException()
        {
            var data = new { RecipientPhone = "+918007333733" };
            var mobileRequest = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone = "+918007333733", Name = "abc" };
            mobileVerificationService.Setup(x => x.Initiate(It.IsAny<string>(), It.IsAny<string>(), mobileRequest)).Throws(new MobileVerificationFailedException());
            var result = (ErrorResult)await smsController.InitiateMobileVerification("123", "123", mobileRequest);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 404);

        }

        [Fact]
        public async void InitiateMobileVerification_Returns_ArgumentNullException()
        {
            var data = new { RecipientPhone = "+918007333733" };
            var mobileRequest = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone = "+918007333733", Name = "abc" };
            mobileVerificationService.Setup(x => x.Initiate(It.IsAny<string>(), It.IsAny<string>(), mobileRequest)).Throws(new ArgumentNullException());
            var result = (ErrorResult)await smsController.InitiateMobileVerification("123", "123", mobileRequest);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

        }

        [Fact]
        public async void InitiateMobileVerification_Returns_FormatException()
        {
            var data = new { RecipientPhone = "+918007333733" };
            var mobileRequest = new MobileVerificationRequest() { Data = data, Mobile = "+918007333733", Phone = "+918007333733", Name = "abc" };
            mobileVerificationService.Setup(x => x.Initiate(It.IsAny<string>(), It.IsAny<string>(), mobileRequest)).Throws(new FormatException());
            var result = (ErrorResult)await smsController.InitiateMobileVerification("123", "123", mobileRequest);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

        }



        [Fact]
        public async void VerifyMobile_Returns_Ok()
        {
           
            var emailresoinse = new VerifyEmailResponse() { VerificationSuccess = true };           
            mobileVerificationService.Setup(x => x.Verify(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emailresoinse);
            var result = (HttpOkObjectResult)await smsController.VerifyMobile("123", "123");

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
            //Assert.True(result.Value.GetType);
        }

        [Fact]
        public async void VerifyMobile_Returns_MobileVerificationException()
        {

            mobileVerificationService.Setup(x => x.Verify(It.IsAny<string>(), It.IsAny<string>())).Throws(new MobileVerificationException());
            var result = (ErrorResult)await smsController.VerifyMobile("123", "123");
         
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

        }

        [Fact]
        public async void VerifyMobile_Returns_MobileVerificationExpiredException()
        {

            mobileVerificationService.Setup(x => x.Verify(It.IsAny<string>(), It.IsAny<string>())).Throws(new MobileVerificationExpiredException());
            var result = (ErrorResult)await smsController.VerifyMobile("123", "123");
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 404);

        }

        [Fact]
        public async void VerifyMobile_Returns_MobileVerificationFailedException()
        {

            mobileVerificationService.Setup(x => x.Verify(It.IsAny<string>(), It.IsAny<string>())).Throws(new MobileVerificationFailedException());
            var result = (ErrorResult)await smsController.VerifyMobile("123", "123");
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 404);

        }

        [Fact]
        public async void VerifyMobile_Returns_ArgumentNullException()
        {

            mobileVerificationService.Setup(x => x.Verify(It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentNullException());
            var result = (ErrorResult)await smsController.VerifyMobile("123", "123");
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

        }

        [Fact]
        public async void VerifyMobile_Returns_Exception()
        {

            mobileVerificationService.Setup(x => x.Verify(It.IsAny<string>(), It.IsAny<string>())).Throws(new Exception());
            var result = (ErrorResult)await smsController.VerifyMobile("123", "123");
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

        }




    }
}
